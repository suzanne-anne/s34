// Initialization
const express = require('express')

const app = express() // Initializing express as the 'app' variable

const port = 3000


// Registration
app.use(express.json()) // express.json() middleware allows our application to read JSON data

app.use(express.urlencoded({extended:true})) // urlencoded middleware allows our application to read data from forms


// [SECTION] Routes
app.get('/', (request, response) => {
	response.send('Hello World!')
})

app.post('/hello', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
})


let users = []; // Mock database

app.post('/register', (request, response) => {

	// Validation process
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body)
		
		response.send(users)
	} else {
		response.send("Please input BOTH username and password >:(")
	}
})

app.patch("/users/change-password", (request, response) => {
	// Placeholder for the final message of this functionality
	let message;

	for(let i=0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password 

			message = `User ${request.body.username}'s password has been changed!`

			break 
		} else {
			message = "User does not exist :("
		}
	}

	response.send(message)
})


// Listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`))